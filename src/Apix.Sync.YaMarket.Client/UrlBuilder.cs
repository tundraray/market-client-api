﻿﻿﻿using System;
using System.Net;

namespace Apix.Sync.YaMarket
{
    internal class UrlBuilder
    {
        private const string apiUrl = "https://mobile.market.yandex.net/market/content/v2.0.1/";

        private const string OffersUrl = "https://market.yandex.ru/api/product--{1}/{0}/offers?local-offers-first=1&how=aprice&included-in-price=delivery&pageId=market%3Aproduct-offers&productId={0}&cvredirect=0&refererPageId=offers";
        
        private const string OffersOldUrl = "models/{1}/offers?&page=1&count={2}&sort=PRICE&how=ASC&groupBy=SHOP&show_discounts=1&fields=OFFER_DISCOUNT%2COFFER_SHOP%2COFFER_DELIVERY%2CSHOP_RATING%2COFFER_VENDOR%2CVENDOR_ALL%2COFFER_PHOTO&uuid={0}";
        private const string OffersOldWithLocationUrl = "models/{1}/offers?&page=1&count={2}&sort=PRICE&how=ASC&groupBy=SHOP&show_discounts=1&fields=OFFER_DISCOUNT%2COFFER_SHOP%2COFFER_DELIVERY%2CSHOP_RATING%2COFFER_VENDOR%2CVENDOR_ALL%2COFFER_PHOTO&uuid={0}&latitude={3}&longitude={4}";
        private const string DetailsUrl = "https://mobile.market.yandex.net/market/content/v1/model/{1}/details.json?&sections=medicine&lac=7837&cellid=8299972&operatorid=2&countrycode=250&signalstrength=0&wifinetworks=FAF082B8DFC4:-45,90F652B145D0:-75,E84DD0EED178:-73,78542EDFBDE5:-68,14CC203F6231:-79,0023CDCE765C:-85&uuid={0}";


        public string Offers( int id, string slug, int size = 5)
        {
            return string.Format(OffersUrl, id, slug, size);
        }

        public string OffersOld(int id, int size = 5)
        {
            return string.Format("{0}{1}", apiUrl, string.Format(OffersOldUrl, Guid.NewGuid().ToString("N"), id, size));
        }
        public string OffersOld(int id, string latitude, string longitude, int size = 5)
        {
            return string.Format("{0}{1}", apiUrl, string.Format(OffersOldWithLocationUrl, Guid.NewGuid().ToString("N"), id, size, latitude, longitude));
        }

        public string Details(int id)
        {
            return string.Format(DetailsUrl, Guid.NewGuid().ToString("N"), id);
        }

        public string Search(string query)
        {
            return string.Format("https://mobile.market.yandex.net/market/content/v2.0.2/redirect?redirect_types=CATALOG,MODEL,PROMO_PAGE,VENDOR,SEARCH&contents=REDIRECT_MODEL,REDIRECT_VENDOR,REDIRECT_CATALOG&count=1&text={0}&lac=7837&cellid=8310723&operatorid=2&countrycode=250&signalstreng&standart&lac=7837&cellid=8310723&operatorid=2&countrycode=250&signalstrength=0&wifinetworks=FAF082B8DFC4:-32,78542EDFBDE5:-61,14CC203F6231:-68,E84DD0EED178:-71,90F652B145D0:-76,0023CDCE765C:-82,E894F66FA4B2:-87&uuid={1}&sections=medicine", WebUtility.UrlEncode( query), Guid.NewGuid().ToString("N"));
        }

        public string ModelInfo(int id)
        {
            return string.Format("https://mobile.market.yandex.net/market/content/v2.0.9/models/{0}?&fields=CATEGORY%2CDEFAULT_OFFER%2CDISCOUNTS%2CFACTS%2CSPECIFICATION%2CPHOTO%2CPHOTOS%2CPRICE%2CRATING%2CMEDIA%2CMODIFICATIONS%2COFFERS%2CFILTERS%2CVENDOR%2CFILTER_ALL%2COFFER_ACTIVE_FILTERS%2COFFER_CATEGORY%2COFFER_DELIVERY%2COFFER_DISCOUNT%2COFFER_PHOTO%2COFFER_SHOP%2COFFER_VENDOR%2COFFER_BUNDLE_SETTINGS%2CSHOP_ALL&sections=medicine&lac=7837&cellid=8310723&operatorid=2&countrycode=250&signalstrength=0&wifinetworks=FAF082B8DFC4:-40,78542EDFBDE5:-65,14CC203F6231:-76,E84DD0EED178:-70,B0487AC3551A:-88,1062EB1B6F3E:-88&uuid={1}&latitude=59.870522&longitude=30.367132", id, Guid.NewGuid().ToString("N"));
        }
    }
}