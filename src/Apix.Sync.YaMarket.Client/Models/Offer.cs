﻿// To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
//
//    using Apix.Sync.YaMarket.Models;
//
//    var welcome = Welcome.FromJson(jsonString);

namespace Apix.Sync.YaMarket.Models
{
    using System;
    using System.Collections.Generic;

    using System.Globalization;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public partial class OfferResponseModel
    {
        [JsonProperty("offersList")]
        public SearchResponseModel OfferData { get; set; }

        [JsonProperty("success")]
        public bool Status { get; set; }
    }

    public partial class SearchResponseModel
    {
        [JsonProperty("search")]
        public OfferData OfferData { get; set; }

    }

    public partial class OfferData
    {

        [JsonProperty("results")]
        public List<Offer> Offers { get; set; }
    }

    public partial class Benefit
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("description")]
        public string Description { get; set; }

        [JsonProperty("isPrimary")]
        public bool IsPrimary { get; set; }
    }

    public partial class BundleSettings
    {
        [JsonProperty("quantityLimit")]
        public QuantityLimit QuantityLimit { get; set; }
    }

    public partial class QuantityLimit
    {
        [JsonProperty("minimum")]
        public int Minimum { get; set; }

        [JsonProperty("step")]
        public int Step { get; set; }
    }

    public partial class CategoryElement
    {
        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("cpaType")]
        public string CpaType { get; set; }

        [JsonProperty("isLeaf")]
        public bool IsLeaf { get; set; }
    }

    public partial class OfferDelivery
    {
        [JsonProperty("shopPriorityRegion")]
        public Region ShopPriorityRegion { get; set; }

        [JsonProperty("shopPriorityCountry")]
        public Region ShopPriorityCountry { get; set; }

        [JsonProperty("isPriorityRegion")]
        public bool IsPriorityRegion { get; set; }

        [JsonProperty("isCountrywide")]
        public bool IsCountrywide { get; set; }

        [JsonProperty("isAvailable")]
        public bool IsAvailable { get; set; }

        [JsonProperty("hasPickup")]
        public bool HasPickup { get; set; }

        [JsonProperty("hasLocalStore")]
        public bool HasLocalStore { get; set; }

        [JsonProperty("hasPost")]
        public bool HasPost { get; set; }

        [JsonProperty("region")]
        public Region Region { get; set; }

        [JsonProperty("availableServices")]
        public AvailableService[] AvailableServices { get; set; }

        [JsonProperty("price")]
        public OptionPrice Price { get; set; }

        [JsonProperty("isFree")]
        public bool IsFree { get; set; }

        [JsonProperty("isDownloadable")]
        public bool IsDownloadable { get; set; }

        [JsonProperty("inStock")]
        public bool InStock { get; set; }

        [JsonProperty("postAvailable")]
        public bool PostAvailable { get; set; }

        [JsonProperty("options")]
        public DeliveryOption[] Options { get; set; }

        [JsonProperty("pickupOptions", NullValueHandling = NullValueHandling.Ignore)]
        public PickupOption[] PickupOptions { get; set; }
    }

    public partial class AvailableService
    {
        [JsonProperty("serviceId")]
        public int ServiceId { get; set; }

        [JsonProperty("serviceName")]
        public string ServiceName { get; set; }
    }

    public partial class DeliveryOption
    {
        [JsonProperty("price")]
        public OptionPrice Price { get; set; }

        [JsonProperty("isDefault")]
        public bool IsDefault { get; set; }

        [JsonProperty("serviceId")]
        public string ServiceId { get; set; }

        [JsonProperty("paymentMethods", NullValueHandling = NullValueHandling.Ignore)]
        public string[] PaymentMethods { get; set; }

        [JsonProperty("partnerType")]
        public string PartnerType { get; set; }

        [JsonProperty("region")]
        public Region Region { get; set; }

        [JsonProperty("dayFrom", NullValueHandling = NullValueHandling.Ignore)]
        public int? DayFrom { get; set; }

        [JsonProperty("dayTo", NullValueHandling = NullValueHandling.Ignore)]
        public int? DayTo { get; set; }
    }

    public partial class OptionPrice
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("isDeliveryIncluded")]
        public bool IsDeliveryIncluded { get; set; }
    }

    public partial class Region
    {
        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

    }



    public partial class PickupOption
    {
        [JsonProperty("serviceId")]
        public int ServiceId { get; set; }

        [JsonProperty("serviceName")]
        public string ServiceName { get; set; }

        [JsonProperty("price")]
        public PickupOptionPrice Price { get; set; }

        [JsonProperty("groupCount")]
        public int GroupCount { get; set; }

        [JsonProperty("region")]
        public Region Region { get; set; }
    }

    public partial class PickupOptionPrice
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class OfferFilter
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("xslname")]
        public string Xslname { get; set; }

        [JsonProperty("subType")]
        public string SubType { get; set; }

        [JsonProperty("kind")]
        public int Kind { get; set; }

        [JsonProperty("position")]
        public int Position { get; set; }

        [JsonProperty("noffers")]
        public int Noffers { get; set; }

        [JsonProperty("valuesCount")]
        public int ValuesCount { get; set; }

        [JsonProperty("values")]
        public PurpleValue[] Values { get; set; }

        [JsonProperty("valuesGroups")]
        public ValuesGroup[] ValuesGroups { get; set; }
    }

    public partial class PurpleValue
    {
        [JsonProperty("initialFound")]
        public int InitialFound { get; set; }

        [JsonProperty("group")]
        public string Group { get; set; }

        [JsonProperty("found")]
        public int Found { get; set; }

        [JsonProperty("value")]
        public string Value { get; set; }

        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }

    public partial class ValuesGroup
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("valuesIds")]
        public List<string> ValuesIds { get; set; }
    }

    public partial class Manufacturer
    {
        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("warranty")]
        public bool Warranty { get; set; }

        [JsonProperty("country", NullValueHandling = NullValueHandling.Ignore)]
        public string Country { get; set; }

        [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
        public string Code { get; set; }
    }

    public partial class RootNavnodeClass
    {
    }

    public partial class Model
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }

    public partial class Navnode
    {
        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("fullName")]
        public string FullName { get; set; }

        [JsonProperty("isLeaf")]
        public bool IsLeaf { get; set; }

        [JsonProperty("rootNavnode")]
        public RootNavnodeClass RootNavnode { get; set; }
    }

    public partial class Outlet
    {
        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("paymentMethods", NullValueHandling = NullValueHandling.Ignore)]
        public string[] PaymentMethods { get; set; }

        [JsonProperty("serviceId")]
        public int ServiceId { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("isMarketBranded")]
        public bool IsMarketBranded { get; set; }

        [JsonProperty("shop")]
        public Model Shop { get; set; }

        [JsonProperty("address")]
        public Dictionary<string, string> Address { get; set; }

        [JsonProperty("telephones")]
        public Telephone[] Telephones { get; set; }

        [JsonProperty("workingTime")]
        public WorkingTime[] WorkingTime { get; set; }

        [JsonProperty("selfDeliveryRule")]
        public SelfDeliveryRule SelfDeliveryRule { get; set; }

        [JsonProperty("region")]
        public Region Region { get; set; }

        [JsonProperty("gpsCoord")]
        public GpsCoord GpsCoord { get; set; }
    }

    public partial class GpsCoord
    {
        [JsonProperty("longitude")]
        public string intitude { get; set; }

        [JsonProperty("latitude")]
        public string Latitude { get; set; }
    }

    public partial class SelfDeliveryRule
    {
        [JsonProperty("workInHoliday")]
        public bool WorkInHoliday { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("cost")]
        public string Cost { get; set; }

        [JsonProperty("shipperHumanReadableId")]
        public string ShipperHumanReadableId { get; set; }
    }

    public partial class Telephone
    {
        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }

        [JsonProperty("cityCode")]
        public string CityCode { get; set; }

        [JsonProperty("telephoneNumber")]
        public string TelephoneNumber { get; set; }

        [JsonProperty("extensionNumber")]
        public string ExtensionNumber { get; set; }
    }

    public partial class WorkingTime
    {
        [JsonProperty("daysFrom")]
        public string DaysFrom { get; set; }

        [JsonProperty("daysTo")]
        public string DaysTo { get; set; }

        [JsonProperty("hoursFrom")]
        public string HoursFrom { get; set; }

        [JsonProperty("hoursTo")]
        public string HoursTo { get; set; }
    }

    public partial class Picture
    {
        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("original")]
        public Original Original { get; set; }

        [JsonProperty("thumbnails")]
        public Original[] Thumbnails { get; set; }

        [JsonProperty("signatures")]
        public object[] Signatures { get; set; }
    }

    public partial class Original
    {
        [JsonProperty("containerWidth")]
        public int ContainerWidth { get; set; }

        [JsonProperty("containerHeight")]
        public int ContainerHeight { get; set; }

        [JsonProperty("url")]
        public string Url { get; set; }

        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }
    }

    public partial class Prices
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("value")]
        [JsonConverter(typeof(ParseStringConverter))]
        public int Value { get; set; }

        [JsonProperty("isDeliveryIncluded")]
        public bool IsDeliveryIncluded { get; set; }

        [JsonProperty("rawValue")]
        [JsonConverter(typeof(ParseStringConverter))]
        public int RawValue { get; set; }

        [JsonProperty("discount", NullValueHandling = NullValueHandling.Ignore)]
        public Discount Discount { get; set; }
    }

    public partial class Discount
    {
        [JsonProperty("oldMin")]
        [JsonConverter(typeof(ParseStringConverter))]
        public int OldMin { get; set; }

        [JsonProperty("percent")]
        public int Percent { get; set; }

        [JsonProperty("isBestDeal")]
        public bool IsBestDeal { get; set; }
    }

    public partial class Seller
    {
        [JsonProperty("comment", NullValueHandling = NullValueHandling.Ignore)]
        public string Comment { get; set; }

        [JsonProperty("price")]
        [JsonConverter(typeof(ParseStringConverter))]
        public int Price { get; set; }

        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("sellerToUserExchangeRate")]
        public int SellerToUserExchangeRate { get; set; }
    }

    public partial class Shop
    {
        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("gradesCount")]
        public int GradesCount { get; set; }

        [JsonProperty("overallGradesCount")]
        public int OverallGradesCount { get; set; }

        [JsonProperty("qualityRating")]
        public int QualityRating { get; set; }

        [JsonProperty("isGlobal")]
        public bool IsGlobal { get; set; }

        [JsonProperty("isCpaPrior")]
        public bool IsCpaPrior { get; set; }

        [JsonProperty("isCpaPartner")]
        public bool IsCpaPartner { get; set; }

        [JsonProperty("isNewRating")]
        public bool IsNewRating { get; set; }

        [JsonProperty("newGradesCount")]
        public int NewGradesCount { get; set; }

        [JsonProperty("newQualityRating")]
        public double NewQualityRating { get; set; }

        [JsonProperty("newQualityRating3M")]
        public double NewQualityRating3M { get; set; }

        [JsonProperty("ratingToShow")]
        public double RatingToShow { get; set; }

        [JsonProperty("newGradesCount3M")]
        public int NewGradesCount3M { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("cutoff")]
        public string Cutoff { get; set; }

        [JsonProperty("outletsCount")]
        public int OutletsCount { get; set; }

        [JsonProperty("storesCount")]
        public int StoresCount { get; set; }

        [JsonProperty("pickupStoresCount")]
        public int PickupStoresCount { get; set; }

        [JsonProperty("depotStoresCount")]
        public int DepotStoresCount { get; set; }

        [JsonProperty("postomatStoresCount")]
        public int PostomatStoresCount { get; set; }

        [JsonProperty("bookNowStoresCount")]
        public int BookNowStoresCount { get; set; }

        [JsonProperty("subsidies")]
        public bool Subsidies { get; set; }

        [JsonProperty("feed")]
        public Feed Feed { get; set; }

        [JsonProperty("phones")]
        public Phones Phones { get; set; }

        [JsonProperty("homeRegion")]
        public HomeRegion HomeRegion { get; set; }

        [JsonProperty("taxSystem", NullValueHandling = NullValueHandling.Ignore)]
        public string TaxSystem { get; set; }

        [JsonProperty("deliveryVat", NullValueHandling = NullValueHandling.Ignore)]
        public string DeliveryVat { get; set; }
    }

    public partial class Feed
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("offerId")]
        public string OfferId { get; set; }

        [JsonProperty("categoryId")]
        public string CategoryId { get; set; }
    }

    public partial class HomeRegion
    {
        [JsonProperty("entity")]
        public string Entity { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("lingua")]
        public HomeRegionLingua Lingua { get; set; }
    }

    public partial class HomeRegionLingua
    {
        [JsonProperty("name")]
        public RootNavnodeClass Name { get; set; }
    }

    public partial class Phones
    {
        [JsonProperty("raw")]
        public string Raw { get; set; }

        [JsonProperty("sanitized")]
        public string Sanitized { get; set; }
    }

    public partial class Titles
    {
        [JsonProperty("raw")]
        public string Raw { get; set; }

        [JsonProperty("highlighted")]
        public Highlighted[] Highlighted { get; set; }
    }

    public partial class Highlighted
    {
        [JsonProperty("value")]
        public string Value { get; set; }
    }

    public partial class OfferUrls
    {
        [JsonProperty("encrypted")]
        public string Encrypted { get; set; }

        [JsonProperty("geo")]
        public string Geo { get; set; }

        [JsonProperty("callPhone")]
        public string CallPhone { get; set; }

        [JsonProperty("direct")]
        public string Direct { get; set; }
    }

    public partial class Vendor
    {
        [JsonProperty("shops")]
        public object[] Shops { get; set; }

        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public partial class DataMeta
    {
        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("shops")]
        public int Shops { get; set; }
    }

    public partial class Offer
    {
        [JsonProperty("showUid", NullValueHandling = NullValueHandling.Ignore)]
        public string ShowUid { get; set; }



        [JsonProperty("vendor", NullValueHandling = NullValueHandling.Ignore)]
        public Vendor Vendor { get; set; }

        [JsonProperty("titles", NullValueHandling = NullValueHandling.Ignore)]
        public Titles Titles { get; set; }

        [JsonProperty("slug", NullValueHandling = NullValueHandling.Ignore)]
        public string Slug { get; set; }

        [JsonProperty("description", NullValueHandling = NullValueHandling.Ignore)]
        public string Description { get; set; }

        [JsonProperty("eligibleForBookingInUserRegion", NullValueHandling = NullValueHandling.Ignore)]
        public bool? EligibleForBookingInUserRegion { get; set; }

        [JsonProperty("categories", NullValueHandling = NullValueHandling.Ignore)]
        public List<CategoryElement> Categories { get; set; }

        [JsonProperty("cpc", NullValueHandling = NullValueHandling.Ignore)]
        public string Cpc { get; set; }


        [JsonProperty("navnodes", NullValueHandling = NullValueHandling.Ignore)]
        public List<Navnode> Navnodes { get; set; }

        [JsonProperty("pictures", NullValueHandling = NullValueHandling.Ignore)]
        public List<Picture> Pictures { get; set; }



        [JsonProperty("model", NullValueHandling = NullValueHandling.Ignore)]
        public Model Model { get; set; }

        [JsonProperty("isCutPrice", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsCutPrice { get; set; }

        [JsonProperty("delivery", NullValueHandling = NullValueHandling.Ignore)]
        public ResultDelivery Delivery { get; set; }

        [JsonProperty("shop", NullValueHandling = NullValueHandling.Ignore)]
        public Shop Shop { get; set; }

        [JsonProperty("wareId", NullValueHandling = NullValueHandling.Ignore)]
        public string WareId { get; set; }

        [JsonProperty("classifierMagicId", NullValueHandling = NullValueHandling.Ignore)]
        public string ClassifierMagicId { get; set; }

        [JsonProperty("prices", NullValueHandling = NullValueHandling.Ignore)]
        public Prices Prices { get; set; }


        [JsonProperty("seller", NullValueHandling = NullValueHandling.Ignore)]
        public Seller Seller { get; set; }


        [JsonProperty("isRecommendedByVendor", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsRecommendedByVendor { get; set; }

        [JsonProperty("bundleCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? BundleCount { get; set; }

        [JsonProperty("bundled", NullValueHandling = NullValueHandling.Ignore)]
        public Bundled Bundled { get; set; }

        [JsonProperty("outlet", NullValueHandling = NullValueHandling.Ignore)]
        public Outlet Outlet { get; set; }

        [JsonProperty("bundleSettings", NullValueHandling = NullValueHandling.Ignore)]
        public BundleSettings BundleSettings { get; set; }

        [JsonProperty("prepayEnabled", NullValueHandling = NullValueHandling.Ignore)]
        public bool? PrepayEnabled { get; set; }

        [JsonProperty("promoCodeEnabled", NullValueHandling = NullValueHandling.Ignore)]
        public bool? PromoCodeEnabled { get; set; }

        [JsonProperty("feedGroupId", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public int? FeedGroupId { get; set; }

        [JsonProperty("isFulfillment", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsFulfillment { get; set; }

        [JsonProperty("isDailyDeal", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsDailyDeal { get; set; }

        [JsonProperty("isAdult", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsAdult { get; set; }

        [JsonProperty("isGoldenMatrix", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsGoldenMatrix { get; set; }

        [JsonProperty("shouldShowCpcInPopup")]
        public bool ShouldShowCpcInPopup { get; set; }

        [JsonProperty("isForeign", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsForeign { get; set; }

        [JsonProperty("titlesWithoutVendor", NullValueHandling = NullValueHandling.Ignore)]
        public Titles TitlesWithoutVendor { get; set; }

        [JsonProperty("weight", NullValueHandling = NullValueHandling.Ignore)]
        public string Weight { get; set; }
    }

    public partial class OffersSetUrls
    {
        [JsonProperty("encrypted")]
        public string Encrypted { get; set; }

        [JsonProperty("geo")]
        public string Geo { get; set; }

        [JsonProperty("pickupGeo")]
        public string PickupGeo { get; set; }

        [JsonProperty("storeGeo")]
        public string StoreGeo { get; set; }

        [JsonProperty("postomatGeo")]
        public string PostomatGeo { get; set; }

        [JsonProperty("showPhone")]
        public string ShowPhone { get; set; }

        [JsonProperty("offercard")]
        public string Offercard { get; set; }

        [JsonProperty("direct")]
        public string Direct { get; set; }
    }

    public partial class Payload
    {
        [JsonProperty("sorts")]
        public Sort[] Sorts { get; set; }

        [JsonProperty("search")]
        public Search Search { get; set; }

        [JsonProperty("filters")]
        public PayloadFilter[] Filters { get; set; }
    }

    public partial class PayloadFilter
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("subType", NullValueHandling = NullValueHandling.Ignore)]
        public string SubType { get; set; }

        [JsonProperty("kind")]
        public int Kind { get; set; }


        [JsonProperty("hasBoolNo", NullValueHandling = NullValueHandling.Ignore)]
        public bool? HasBoolNo { get; set; }

        [JsonProperty("valuesCount", NullValueHandling = NullValueHandling.Ignore)]
        public int? ValuesCount { get; set; }

        [JsonProperty("valuesGroups", NullValueHandling = NullValueHandling.Ignore)]
        public object[] ValuesGroups { get; set; }
    }

    public partial class FluffyValue
    {
        [JsonProperty("max", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public int? Max { get; set; }

        [JsonProperty("initialMax", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public int? InitialMax { get; set; }

        [JsonProperty("initialMin", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public int? InitialMin { get; set; }

        [JsonProperty("min", NullValueHandling = NullValueHandling.Ignore)]
        [JsonConverter(typeof(ParseStringConverter))]
        public int? Min { get; set; }

        [JsonProperty("id", NullValueHandling = NullValueHandling.Ignore)]
        public string Id { get; set; }

        [JsonProperty("found", NullValueHandling = NullValueHandling.Ignore)]
        public int? Found { get; set; }

        [JsonProperty("value", NullValueHandling = NullValueHandling.Ignore)]
        public string Value { get; set; }

        [JsonProperty("initialFound", NullValueHandling = NullValueHandling.Ignore)]
        public int? InitialFound { get; set; }

        [JsonProperty("checked", NullValueHandling = NullValueHandling.Ignore)]
        public bool? Checked { get; set; }
    }

    public partial class Search
    {
        [JsonProperty("groupBy")]
        public string GroupBy { get; set; }

        [JsonProperty("total")]
        public int Total { get; set; }

        [JsonProperty("totalOffers")]
        public int TotalOffers { get; set; }

        [JsonProperty("totalOffersBeforeFilters")]
        public int TotalOffersBeforeFilters { get; set; }

        [JsonProperty("totalModels")]
        public int TotalModels { get; set; }

        [JsonProperty("adult")]
        public bool Adult { get; set; }

        [JsonProperty("salesDetected")]
        public bool SalesDetected { get; set; }

        [JsonProperty("shops")]
        public int Shops { get; set; }

        [JsonProperty("totalShopsBeforeFilters")]
        public int TotalShopsBeforeFilters { get; set; }

        [JsonProperty("shopOutlets")]
        public int ShopOutlets { get; set; }

        [JsonProperty("cpaCount")]
        public int CpaCount { get; set; }

        [JsonProperty("isParametricSearch")]
        public bool IsParametricSearch { get; set; }

        [JsonProperty("duplicatesHidden")]
        public int DuplicatesHidden { get; set; }

        [JsonProperty("category")]
        public SearchCategory Category { get; set; }

        [JsonProperty("isDeliveryIncluded")]
        public bool IsDeliveryIncluded { get; set; }

        [JsonProperty("showBlockId")]
        public string ShowBlockId { get; set; }

        [JsonProperty("results")]
        public List<Offer> Results { get; set; }
    }

    public partial class SearchCategory
    {
        [JsonProperty("cpaType")]
        public string CpaType { get; set; }
    }


    public partial class Bundled
    {
        [JsonProperty("modelId")]
        public int ModelId { get; set; }

        [JsonProperty("count")]
        public int Count { get; set; }
    }

    public partial class ResultDelivery
    {
        [JsonProperty("shopPriorityRegion")]
        public Region ShopPriorityRegion { get; set; }

        [JsonProperty("shopPriorityCountry")]
        public Region ShopPriorityCountry { get; set; }

        [JsonProperty("isPriorityRegion")]
        public bool IsPriorityRegion { get; set; }

        [JsonProperty("isCountrywide")]
        public bool IsCountrywide { get; set; }

        [JsonProperty("isAvailable")]
        public bool IsAvailable { get; set; }

        [JsonProperty("hasPickup")]
        public bool HasPickup { get; set; }

        [JsonProperty("hasLocalStore")]
        public bool HasLocalStore { get; set; }

        [JsonProperty("hasPost")]
        public bool HasPost { get; set; }

        [JsonProperty("region")]
        public Region Region { get; set; }

        [JsonProperty("availableServices")]
        public AvailableService[] AvailableServices { get; set; }

        [JsonProperty("price", NullValueHandling = NullValueHandling.Ignore)]
        public OptionPrice Price { get; set; }

        [JsonProperty("isFree")]
        public bool IsFree { get; set; }

        [JsonProperty("isDownloadable")]
        public bool IsDownloadable { get; set; }

        [JsonProperty("inStock")]
        public bool InStock { get; set; }

        [JsonProperty("postAvailable")]
        public bool PostAvailable { get; set; }

        [JsonProperty("options")]
        public DeliveryOption[] Options { get; set; }
    }

    public partial class Sort
    {
        [JsonProperty("text")]
        public string Text { get; set; }

        [JsonProperty("options", NullValueHandling = NullValueHandling.Ignore)]
        public SortOption[] Options { get; set; }
    }

    public partial class SortOption
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("type", NullValueHandling = NullValueHandling.Ignore)]
        public string Type { get; set; }

        [JsonProperty("isActive", NullValueHandling = NullValueHandling.Ignore)]
        public bool? IsActive { get; set; }
    }

    internal static class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
            Converters = {
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
        };
    }

    internal class ParseStringConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long) || t == typeof(long?);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            if (reader.TokenType == JsonToken.Null) return null;
            var value = serializer.Deserialize<string>(reader);
            int l;
            if (Int32.TryParse(value, out l))
            {
                return l;
            }
            return 0;
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            if (untypedValue == null)
            {
                serializer.Serialize(writer, null);
                return;
            }
            var value = (long)untypedValue;
            serializer.Serialize(writer, value.ToString());
            return;
        }

        public static readonly ParseStringConverter Singleton = new ParseStringConverter();
    }

    internal class DecodeArrayConverter : JsonConverter
    {
        public override bool CanConvert(Type t) => t == typeof(long[]);

        public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
        {
            reader.Read();
            var value = new List<long>();
            while (reader.TokenType != JsonToken.EndArray)
            {
                var converter = ParseStringConverter.Singleton;
                var arrayItem = (long)converter.ReadJson(reader, typeof(long), null, serializer);
                value.Add(arrayItem);
                reader.Read();
            }
            return value.ToArray();
        }

        public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
        {
            var value = (long[])untypedValue;
            writer.WriteStartArray();
            foreach (var arrayItem in value)
            {
                var converter = ParseStringConverter.Singleton;
                converter.WriteJson(writer, arrayItem, serializer);
            }
            writer.WriteEndArray();
            return;
        }

        public static readonly DecodeArrayConverter Singleton = new DecodeArrayConverter();
    }

    

}
