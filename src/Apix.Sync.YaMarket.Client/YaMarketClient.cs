﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Apix.Http.Client;
using Apix.Sync.YaMarket.Models;
using Newtonsoft.Json;

namespace Apix.Sync.YaMarket
{
    public class YaMarketClient : HttpClientBase
    {
        private readonly string _token;
        private UrlBuilder UrlBuilder => new UrlBuilder();

        public YaMarketClient(string cookie, int regionId, string token, string referer, ProxySettings proxy = null) : base(
            new Dictionary<string, string>()
            {
                {"sk", token},
                {"User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36"},
                { "Cookie", $"{cookie}; yandexmarket=48%2CRUR%2C1%2C%2C%2C%2C2%2C0%2C0%2C{regionId}%2C0%2C0%2C12%2C0" },
                {"X-Retpath-Y", referer },
                {"Referer", referer }
            }, proxy: proxy)
        {
            _token = token;

            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Converters = Converter.Settings.Converters
            };

        }

        public async Task<List<Offer>> ListOffers(int id, string slug, int count, CancellationToken cancellationToken)
        {
            var operationResult = await HttpClient.GetAsync(UrlBuilder.Offers(id, slug),

                requestParameters: new RequestParameters<OfferResponseModel>
                {
                    OnError = CommonBadResponse<OfferResponseModel>
                });

            return operationResult?.OfferData?.OfferData?.Offers.Where(offer => !string.IsNullOrEmpty(offer.ShowUid)).ToList();
        }

        //[Obsolete("ListOldOffers is deprecated, please use ListOffers instead.")]
        //public async Task<List<Offer>> ListOldOffers(int id, int count, CancellationToken cancellationToken)
        //{
        //    var operationResult = await HttpClient.GetAsync(UrlBuilder.OffersOld(id),

        //        requestParameters: new RequestParameters<OffersResult>
        //        {
        //            OnError = CommonBadResponse<OffersResult>
        //        });

        //    return operationResult?.Offers;
        //}

        //public async Task<List<Offer>> ListOldOffers(int id, int count, string latitude, string longitude, CancellationToken cancellationToken)
        //{
        //    var operationResult = await HttpClient.GetAsync(UrlBuilder.OffersOld(id, latitude, longitude),

        //        requestParameters: new RequestParameters<OffersResult>
        //        {
        //            OnError = CommonBadResponse<OffersResult>
        //        });

        //    return operationResult?.Offers;
        //}

        //public async Task<Content> Search(string query, CancellationToken cancellationToken)
        //{
        //    var operationResult = await HttpClient.GetAsync(UrlBuilder.Search(query),

        //        requestParameters: new RequestParameters<SearchResult>
        //        {
        //            OnError = CommonBadResponse<SearchResult>
        //        });

        //    return operationResult?.Redirects?.Content;
        //}

        //public async Task<List<DetailsGroupModel>> Search(int id, CancellationToken cancellationToken)
        //{
        //    var operationResult = await HttpClient.GetAsync(UrlBuilder.Details(id),

        //        requestParameters: new RequestParameters<DetailsResult>
        //        {
        //            OnError = CommonBadResponse<DetailsResult>
        //        });

        //    return operationResult?.modelDetails;
        //}

        private Task<T> CommonBadResponse<T>(HttpResponseMessage response, CancellationToken cancellationToken)
            where T : class
        {
            switch (response.StatusCode)
            {

                case HttpStatusCode.Conflict:
                case HttpStatusCode.NotFound:
                    return Task.FromResult<T>(null);
                default:
                    DefaultBadResponseAction(response, cancellationToken);
                    break;
            }
            return Task.FromResult<T>(null);
        }
    }
}
